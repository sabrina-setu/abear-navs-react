import React from "react"
import LightNav from '../src/components/LightNav'

export default{
    title:'Light Nav',
    component: LightNav,
}

const Template = (args) => <LightNav {...args} />;

export const Default = Template.bind({});
Default.args = {};
