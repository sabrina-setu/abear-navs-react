import React from "react"
import LeftIconNav from '../src/components/LeftIconNav'

export default{
    title:'Left Icon Nav',
    component: LeftIconNav,
}

const Template = (args) => <LeftIconNav {...args} />;

export const Default = Template.bind({});
Default.args = {};
