import React from "react"
import DefaultNav from '../src/components/DefaultNav.jsx'

export default{
    title:'Default Nav',
    component: DefaultNav,
}

const Template = (args) => <DefaultNav {...args} />;

export const Default = Template.bind({});
Default.args = {};
