import React from "react"
import MinimalNav from '../src/components/MinimalNav'

export default{
    title:'Minimal Nav',
    component: MinimalNav,
}

const Template = (args) => <MinimalNav {...args} />;

export const Default = Template.bind({});
Default.args = {};
