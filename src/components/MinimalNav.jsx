const MinimalNav = () => {
    return (
        <header>
        <nav className=" bg-white brand-container mx-auto rounded-sm mt-7 flex justify-around items-center pt-1">
            <a href="" className=" text-4xl font-bold text-orange-brand  pb-2 pr-8 -mt-1">abear</a>

            <div className=" space-x-9 -ml-8">
            <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fab fa-facebook-f"></i></a>
                <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fab fa-twitter"></i></a>
                <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fab fa-google-plus-g"></i></a>
                <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fas fa-rss"></i></a>
            </div>


            <div className=" flex items-center">
            <div className=" space-x-5 -mr-8">
            <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fab fa-facebook-f"></i></a>
                <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fab fa-twitter"></i></a>
                <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fab fa-google-plus-g"></i></a>
                <a href="" className=" text-gray-400 uppercase text-base font-semibold"><i className="fas fa-rss"></i></a>
            </div>
            </div>
        </nav>
    </header>
    )

}
export default MinimalNav;