const DefaultNav = () => {
    return (
        <header>
        <nav className=" bg-orange-brand brand-container mx-auto rounded-sm mt-7 flex justify-around items-center pt-1">
            <a href="" className=" text-4xl font-bold text-white border-gray-400 border-r-2 pb-2 pr-8 -mt-1">abear</a>

            <div className=" space-x-8 -ml-8">
                <a href="" className=" text-white uppercase text-base font-semibold">Home</a>
                <a href="" className=" text-white uppercase text-base font-semibold">Pages</a>
                <a href="" className=" text-white uppercase text-base font-semibold">categories</a>
                <a href="" className=" text-white uppercase text-base font-semibold border-gray-400 border-r-2 border-l-2 pl-5 pr-5 pt-3.5 pb-3">About</a>
                <a href="" className=" text-white uppercase text-base font-semibold">Contract</a>
            </div>

            <div className=" space-x-5">
                <a href="" className=" text-white uppercase text-base font-semibold"><i className="fab fa-facebook-f"></i></a>
                <a href="" className=" text-white uppercase text-base font-semibold"><i className="fab fa-twitter"></i></a>
                <a href="" className=" text-white uppercase text-base font-semibold"><i className="fab fa-google-plus-g"></i></a>
                <a href="" className=" text-white uppercase text-base font-semibold"><i className="fas fa-rss"></i></a>
            </div>

            <div className=" flex items-center">
                <button className=" text-white font-semibold focus:outline-none">Login</button>
                <p className=" text-white pl-2 pr-2 text-xs">|</p>
                <button className=" text-white font-semibold focus:outline-none">Signup</button>
            </div>
        </nav>
    </header>
    )

}
export default DefaultNav;