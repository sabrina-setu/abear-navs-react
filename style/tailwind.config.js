module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === 'publish',
    content: ['./src/**/*.{js,jsx,ts,tsx}']
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
      orange: {
        brand: '#F26522',
      },
      gray:{
        brand: '#F1F1F1'
      }
    },
  },
},
  variants: {
    extend: {},
  },
  plugins: [],
}
